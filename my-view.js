

export class MyView extends HTMLElement {


    bindDialog() {
        if (this.dialogCfg) {
            let button = this.querySelector(this.dialogCfg.buttonSl);
            let dialog = this.querySelector(this.dialogCfg.dialogSl);
            if (button) {
                button.addEventListener('click', (event) => {
                    dialog.open();
                });
            }
        }
    }

    render() {
        this.bindDialog();
    }

    connectedCallback() {
        this.render();
    }
}